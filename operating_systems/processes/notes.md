# Operating System Instruction Ideas: Processes

## Pre-req
- [Complementary] Programming Basics

## Intro Discussion
- TODO

## Task Managers

## Windows
- Show the Task Manager
- Maybe even ProcessExplorer from the SysInternals suite.
- Start a process. Might be fun to launch a game and see what files it opens.

## Linux
- Pick a graphical task manager
- Start a process.
- Notice similarities to Windows.